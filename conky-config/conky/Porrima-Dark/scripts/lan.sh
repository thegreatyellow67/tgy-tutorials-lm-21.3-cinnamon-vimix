#!/bin/bash

LAN_DEVICE=`ip -br l | awk '$1 !~ "lo|vir|wl" { print $1}'`

if [[ "${LAN_DEVICE}" != "" ]]; then
  echo "${LAN_DEVICE}"
else
  echo "Non presente"
fi

exit
