#!/bin/bash
# --------------------------------------------------

Greeting=$(date +%H)
cat $0 | grep $Greeting | sed 's/# '$Greeting' //'

# --------------------------------------------------
# 00 una buona Notte
# 01 una buona Mattina
# 02 una buona Mattina
# 03 una buona Mattina
# 04 una buona Mattina
# 05 una buona Mattina
# 06 una buona Mattina
# 07 una buona Mattina
# 08 una buona Mattina
# 09 una buona Mattina
# 10 una buona Mattina
# 11 un buon Mezzogiorno
# 12 un buon Mezzogiorno
# 13 un buon Pomeriggio
# 14 un buon Pomeriggio
# 15 un buon Pomeriggio
# 16 un buon Pomeriggio
# 17 un buon Pomeriggio
# 18 una buona Serata
# 19 una buona Serata
# 20 una buona Serata
# 21 una buona Serata
# 22 una buona Serata
# 23 una buona Serata
